angular.module("ContactsApp").service("ContactsService", ["$resource", function ($resource) {
  var _getAll = function () {
    var endpoint = $resource("/api/v1/contacts");

    return endpoint.get().$promise;
  };

  return {
    getContacts: _getAll
  };
}]);
