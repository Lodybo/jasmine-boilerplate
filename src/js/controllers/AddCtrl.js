angular.module("ContactsApp").controller("AddCtrl", ["$scope", "ContactsService", function ($scope) {
  $scope.newContact = [];
  $scope.contacts = [];

  $scope.addContact = function () {
    $scope.contacts.push($scope.newContact);
    $scope.newContact = "";
  }

  // $scope.removeContact = function (name) {
  //   var pos = $scope.contacts.indexOf(name);
  //
  //   $scope.contacts.splice(pos, 1);
  // };
}]);
