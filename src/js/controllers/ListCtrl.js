angular.module("ContactsApp").controller("ListCtrl", ["$scope", "ContactsService", function ($scope, cs) {
  $scope.contacts = [];
  // cs = ContactsService
  cs.getContacts().then(function (response) {
    $scope.contacts = response.contacts;
  });
}]);
