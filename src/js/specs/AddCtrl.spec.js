describe('Testing the Contacts app: Adding contacts', function() {
  var $scope;

  beforeEach(module('ContactsApp'));

  beforeEach(inject(function($rootScope, $controller) {
    $scope = $rootScope.$new();
    $controller('AddCtrl', {$scope: $scope});
  }));

  describe('Adding a new contact', function() {
    it('should add a new contact with the name "Dolf"', function() {
      $scope.newContact = "Dolf";

      $scope.addContact();

      expect($scope.contacts.length).toBe(1);
    });
  });

  
});
