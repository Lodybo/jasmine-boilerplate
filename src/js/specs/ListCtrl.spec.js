describe('Testing the Contacts app: Getting the contacts list', function() {
  var $scope, $httpBackend;

  beforeEach(module('ContactsApp'));

  beforeEach(inject(function($rootScope, $controller, _$httpBackend_) {
    $scope = $rootScope.$new();
    $httpBackend = _$httpBackend_;
    $controller('ListCtrl', {$scope: $scope});
  }));

  describe('Getting a list with contacts', function() {
    it('should send a request for contacts', function() {
      $httpBackend.expect("GET", "/api/v1/contacts").respond(200, {
        "contacts": [
          "Bas",
          "Yvonne",
          "Ilyass",
          "Jasper",
          "Olaf",
          "Steven"
        ]
      });

      $httpBackend.flush();

      expect($scope.contacts.length).toBe(6);
    });
  });
});
