module.exports = function(grunt) {

  grunt.initConfig({
    jasmine: {
      unit: {
        src: ["src/js/*.js", "src/js/controllers/*.js", "src/js/services/*.js"],
        options: {
          outfile: "test/_SpecRunner.html",
          specs: ["src/js/specs/*.js"],
          vendor: ["node_modules/angular/angular.js", "node_modules/angular-mocks/angular-mocks.js", "node_modules/angular-resource/angular-resource.js"],
        }
      },
      cover: {
        src: ["src/js/*.js", "src/js/controllers/*.js", "src/js/services/*.js"],
        options: {
          outfile: "test/_SpecRunner.html",
          specs: ["src/js/specs/*.js"],
          vendor: ["node_modules/angular/angular.js", "node_modules/angular-mocks/angular-mocks.js", "node_modules/angular-resource/angular-resource.js"],
          template: require('grunt-template-jasmine-istanbul'),
          templateOptions: {
            coverage: 'test/coverage/coverage.json',
            report: [
							{
								type: 'html',
								options: {
									dir: 'test/coverage/html'
								}
							},
							{
								type: 'cobertura',
								options: {
									dir: 'test/coverage/cobertura'
								}
							},
							{
								type: 'text-summary'
							}
						]
          }
        }
      }
    },
    copy: {},
    watch: {
      files: ["src/js/*.js"],
      tasks: ['jasmine:noCover']
    },
    connect: {
      server: {
        options: {
          port: 9000,
          base: 'test/'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['connect', 'watch']);

};
