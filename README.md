# Jasmine boilerplate

Een simpele boilerplate voor een [Jasmine testsuite](https://www.npmjs.com/package/grunt-contrib-jasmine), gecombineerd met Istanbul voor code coverage.

## Installatie
Eerst clone je deze repo in een map op je harddrive.
```bash
git clone https://Lodybo@bitbucket.org/Lodybo/jasmine-boilerplate.git .
```
_De "." op het einde geeft aan dat hij de inhoud van deze repository in de huidige map moet clonen. Als je de "." weglaat maakt hij een nieuwe maap aan genaamd "jasmine-boilerplate"._

Dan type je `npm install` en installeert Node alle dependencies.

## Gebruik
Om de unit tests te draaien:
```bash
grunt jasmine:unit
```

Om de code coverage met Istanbul te draaien
```bash
grunt jasmine:cover
```

Om het _SpecRunner.html bestand aan te maken, moet je na de taak die je kiest `:build` achter plaatsen:
```bash
grunt jasmine:unit:build

# of

grunt jasmine:cover:build
```

De _SpecRunner kan je vinden in `test/_SpecRunner.html`, het coverage report kan je vinden in `test/coverage/html/index.html`.
